#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
References:
https://stackoverflow.com/questions/23686398/iterate-a-to-zzz-in-python#comment36393488_23686398

https://stackoverflow.com/questions/7977318/how-to-iterate-over-case-insensitive-sorted-dictionary-items?answertab=votes#tab-top

https://requests-cache.readthedocs.io/en/latest/user_guide.html#installation

https://stackoverflow.com/questions/21160310/training-data-format-for-nltk-punkt

https://stackoverflow.com/questions/34805790/how-to-avoid-nltks-sentence-tokenizer-spliting-on-abbreviations

"""
import sys
import argparse
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
from abbreviation_trainer import ABBV_LIST, CLEAN_ABBV_LIST
from string import ascii_lowercase as ALC
from itertools import chain, product
import string

# ====( Setting our script Arguments )==== #
parser = argparse.ArgumentParser(description="Concordance for Bridgewater")

# Script Args
parser.add_argument(
    "-f",
    "--file",
    required=True,
    help="Path to file for parsing"
)

# Set our local args
args = parser.parse_args()


def clean(word):
    """ don't clean abbrv """
    if word not in ABBV_LIST:
        return word.translate(None, string.punctuation)
    return word


def letter_incrementer(size):
    """ chaining letters """
    letters = []
    for chars in chain(ALC, product(ALC, repeat=2), product(ALC, repeat=size)):
        letters.append(''.join(chars))
    return letters


def main():

    word_map = {}

    # train punkt
    punkt_param = PunktParameters()
    abbreviation = CLEAN_ABBV_LIST
    punkt_param.abbrev_types = set(abbreviation)
    tokenizer = PunktSentenceTokenizer(punkt_param)

    # read input
    fp = open(args.file)
    data = fp.read()

    # make a map
    line_count = 1
    for line in tokenizer.tokenize(data):
        for word in line.split():
            clean_word = clean(word.lower())
            if clean_word not in word_map:
                word_map[clean_word] = [1, '%d' % line_count]
            else:
                word_map[clean_word][0] += 1
                word_map[clean_word][1] += ",%d" % line_count
        line_count += 1

    # build the output
    char = 0
    letters = letter_incrementer(4)
    for i in sorted(word_map, key=str.lower):
        print "%4s. %-30s %-10s" % (letters[char], clean(i), word_map[i])
        char += 1

    sys.exit(0)

if __name__ == '__main__':
    main()
