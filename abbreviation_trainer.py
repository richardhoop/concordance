#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import requests_cache
import logging


def infoplease_table(url):
    """ Grabs abbreviations from table pages on this website """

    r = requests.get(url)
    c = r.content
    if r.from_cache:
        logging.debug("Using cache")
    soup = BeautifulSoup(c, "html.parser")
    table = soup.find_all("table", "sgmltable")
    abbv = []
    for a in table:
        for t in a.find_all('td'):
            if '.' in t.text:
                abbv.append(t.text.rstrip())

    return abbv


def infoplease_key_abbreviations(url):
    """ Grabs abbreviations from list pages on this website """

    r = requests.get(url)
    c = r.content
    if r.from_cache:
        logging.debug("Using cache")
    soup = BeautifulSoup(c, "html.parser")
    content = soup.find_all("div", {"id": "content"})
    abbv = []
    for a in content:
        for t in a.find_all('p'):
            if '.' in t.text:
                abbv.append(t.text.split(' - ')[0].rstrip())

    return abbv

requests_cache.install_cache('abbreviations', expire_after=600)
ABBV_LIST = []
ABBV_LIST.extend(infoplease_key_abbreviations(
    'https://www.infoplease.com/people/world-rulers-country-or-region/profiles/key-abbreviations'))

ABBV_LIST.extend(
    infoplease_table('https://www.infoplease.com/common-abbreviations')
)
ABBV_LIST.extend(
    infoplease_table(
        'https://www.infoplease.com/state-abbreviations-and-state-postal-codes')
)

CLEAN_ABBV_LIST = []
for word in ABBV_LIST:
    CLEAN_ABBV_LIST.append(word.rstrip('.'))
